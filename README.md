Projet Java au sein de l'IUT Paris Descartes

Mise en place d'un service permettant le partage de services entre programmeurs et clients.

Projet fait en solitaire

Règles pour les services :
	- ils doivent implémenter l'interface Service (interface vide implémentant Runnable)
	- la classe principale doit $etre en public
	-ils doivent avoir un constructeur avec une socket en paramètre
	-ils doivent contenir un attribut de type socket 
	-ils doivent impoémenter un méthode static "toStringue" qui indique l'utilité du service
Ces contraintes sont vérifiées par le service grâce aux méthodes réflexives du package reflex de Java.